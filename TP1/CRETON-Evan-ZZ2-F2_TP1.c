#include <stdio.h>
#include <stdlib.h>

/* -------------------------------------------------------------------- */
/*                           Question 1                                 */
/* -------------------------------------------------------------------- */
/* vonNeumann       Donne la prochaine itération du générateur          */
/*                  non-linéaire proposé par John Von Neumann           */
/*                                                                      */
/* En entrée: n un entier donnant la seed                               */
/*                                                                      */
/* En sortie: donne un entier pour la prochaine itération               */
/* -------------------------------------------------------------------- */

int vonNeumann(int n){
    return (n * n / 100) % 10000;
}

/* -------------------------------------------------------------------- */
/*                           Question 2                                 */
/* -------------------------------------------------------------------- */
/* vonNeumannIter   Affiche un nombre d'itération du générateur         */
/*                  non-linéaire proposé par John Von Neumann           */
/*                                                                      */
/* En entrée: n un entier donnant la seed et iter le nombre d'itération */
/*      nombre d'itération que l'on souhaite afficher                   */
/*                                                                      */
/* -------------------------------------------------------------------- */

void vonNeumannIter(int n, int iter){
    for(int i = 0; i< iter; i++){
        n = vonNeumann(n);
        printf("%d - %d\n",i, n);
    }
}

/* -------------------------------------------------------------------- */
/*                           Question 3                                 */
/* -------------------------------------------------------------------- */
/* On en peut pas l'utiliser pour des usages scientifiques car          */
/* on ne peut pas reproduire les résultats                              */
/* The  function  rand() is not reentrant, since it uses hidden state   */
/* that is modified on each call.  This might just be the seed value    */
/* to be used by the next call, or it might be something more elaborate.*/
/* In order to get reproducible behavior in a threaded application,     */
/* this state must be made explicit; this can be done using             */
/*  the reentrant function rand_r().                                    */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/*                           Question 4                                 */
/* -------------------------------------------------------------------- */
/* coinToss         Donne le nombre de piles et de faces d'une          */
/*                  simulation d'un lancer de pièces                    */
/*                                                                      */
/* En entrée: iter un entier donnant le nombre d'itération choisis      */
/*                                                                      */
/* -------------------------------------------------------------------- */
/* On obtient le même nombre de piles et de faces à chaque              */
/* fois qu'on relance.                                                  */
/* -------------------------------------------------------------------- */

void coinToss(int iter){
    int nbPiles = 0;
    int nbFaces = 0;

    for(int i = 0; i < iter; i++){
        int resul = rand()%2;
        if(resul){
            nbPiles++;
        }
        else{
            nbFaces++;
        }
    }
    printf("Après %d iteration, on a %d piles et %d faces\n", iter, nbPiles, nbFaces);
}

/* -------------------------------------------------------------------- */
/*                           Question 5/6                               */
/* -------------------------------------------------------------------- */
/* afficheTableau    Affiche le contenu d'un tableau de taille N        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            N la taille du tableau                                    */
/* -------------------------------------------------------------------- */

void afficheTableau(float tab[], int N){
    printf("| ");
    for(int i  = 0; i < N; i++){
        printf("%f | ", tab[i]);
    }
    printf("\n\n");
}

/* -------------------------------------------------------------------- */
/* diceRoll      Met la fréquence que la ième face soit tombée dans une */
/*               simulation d'un dé à N faces dans un tableau à N cases */
/*                                                                      */
/* En entrée: iter un entier donnant le nombre d'itération choisis      */
/*            faces un entier donnant le nombre de faces du dé          */
/*            frequences un tableau d'entier qui contiendra             */
/*            les fréquences d'apparition                               */
/*                                                                      */
/* -------------------------------------------------------------------- */

void diceRoll(int faces, int iter, float frequences[]){
    for(int i = 0; i < faces; i++) frequences[i] = 0;
    for(int i = 0; i < iter; i++){
        frequences[rand()%faces] += 1;
    }
    for(int i = 0; i < faces; i++) frequences[i] /= iter;
    printf("%d itérations, %d faces :\n", iter, faces);
    afficheTableau(frequences, faces);
}

/* -------------------------------------------------------------------- */
/*                           Question 7                                 */
/* -------------------------------------------------------------------- */
/* intRand       Renvoie un entier y tel que y = (ax + c) modulo m      */
/*                                                                      */
/* En entrée: les entiers a, x, c et m                                  */
/*                                                                      */
/* En sortie: l'entier y                                                */
/* -------------------------------------------------------------------- */

int intRand(int x0, int a, int c, int m){
    return (a * x0 + c) % m;
}

/* -------------------------------------------------------------------- */
/* floatRand       Renvoie un flottant y tel que                        */
/*                 y = ((ax + c) modulo m )/ m                          */
/*                                                                      */
/* En entrée: les entiers a, x, c et m                                  */
/*                                                                      */
/* En sortie: le flottant                                               */
/* -------------------------------------------------------------------- */

float floatRand(int x0, int a, int c, int m){
    return intRand(x0, a, c, m)/m;
}

/* -------------------------------------------------------------------- */
/*                           Question 9                                 */
/* -------------------------------------------------------------------- */
/* Le tuple m nombre premier, c égale à 0 et a est un nombre            */
/* premier quand il est modulo m                                        */
/*                                                                      */
/* Le tuple m est un multiple de 2, c égale à 0 et a = 3 modulo 8       */
/*                                                                      */
/* Le tuple m est un multiple de 2, c différent de 0,                   */
/*     c et m sont co-premier                                           */
/*     a-1 est divisible par tous les facteurs premiers de m            */
/*     a-1 est divisble par 4 si m est divisible par 4                  */
/* -------------------------------------------------------------------- */


/* -------------------------------------------------------------------- */
/*                           Question 10                                */
/* -------------------------------------------------------------------- */
/* GNU Scientific Library (GSL)                                         */
/* SIMD-oriented Fast Mersenne Twister (SFMT)                           */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/*                           Question 11                                */
/* -------------------------------------------------------------------- */
/* TestU01                                                              */
/* -------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */
/*                           Question 12                                */
/* -------------------------------------------------------------------- */
/* Tausworthe           Renvoie le prochain nombre de la série          */
/*                      suivant un générateur Tausworthe avec comme     */
/*                      polynome caractéristique x⁴+x+1                 */
/* -------------------------------------------------------------------- */

unsigned int Tausworthe(unsigned int seed){
    return (seed << 1 ^ seed) & 0b1111;
}

int main(){
    return 0;
}
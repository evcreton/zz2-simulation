#ifndef ARRAYSIMU_H
#define ARRAYSIMU_H

#include <stdio.h>
#include <math.h>

void afficheTableauDouble(double tab[], int size);

void afficheTableauInt(int tab[], int size);

double meanArrayDouble(double arrayOccurences[], int size, int drawings);

double meanArrayOccur(int arrayOccurences[], int size, int drawings, double valDep, double pas);

double standDeviation(double tab[], int size, int drawings);

double standDeviationOccur(int tab[], int size, int drawings, double valDep, double pas);

#endif
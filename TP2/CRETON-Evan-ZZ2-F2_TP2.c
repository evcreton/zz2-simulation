#include "TP2.h"
#include "mt19937ar.h"
#include "arraySimu.h"

/* -------------------------------------------------------------------- */
/*                           Question 2                                 */
/* -------------------------------------------------------------------- */
/* uniform    Renvoie un réel aléatoire compris entre a et b            */
/*                                                                      */
/* En entrée: inA un nombre réel                                        */
/*            inB un nombre réel                                        */
/*                                                                      */
/* En sortie: un réel compris entre a et b                              */
/* -------------------------------------------------------------------- */

double uniform(double inA, double inB){
    return inA + (inB - inA) * genrand_real1();
}

/* -------------------------------------------------------------------- */
/*                           Question 3-a                               */
/* -------------------------------------------------------------------- */
/* simulate    Renvoie un tableau de pourcentages des                   */
/*             individues appartenants aux classes A,B,C après          */
/*             un nombre de simulation choisies                         */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*                                                                      */
/* En sortie: le tableau de pourcentages suite à la simulation          */
/* -------------------------------------------------------------------- */

double * simulate(int drawings){
    double * simulation = (double*) calloc(3,sizeof(double));
    double genProb;

    for(int i = 0; i < drawings; i++){
        genProb = genrand_real1();
        if(genProb < 0.35){
            simulation[0] +=1;
        }
        else if (genProb < 0.8){
            simulation[1] +=1;
        }
        else simulation[2] +=1;
    }
    for(int j = 0; j < 3; j++) simulation[j] /= drawings;
    return simulation;
}

/* -------------------------------------------------------------------- */
/*                           Question 3-b                               */
/* -------------------------------------------------------------------- */
/* simGeneral  Renvoie un tableau de pourcentages des                   */
/*             individues appartenants aux classes après                */
/*             un nombre de simulation choisies                         */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*            arrayNumberindividuals contient le nombre d'individues    */
/*                                   appartenant à chaque classe        */
/*            size la taille du tableau                                 */
/*                                                                      */
/* En sortie: le tableau de pourcentages suite à la simulation          */
/* -------------------------------------------------------------------- */

double * simGeneral(int drawings, int size, double * arrayNumberindividuals){
    int totalIndividuals = 0;
    for(int i = 0; i < size; i++){
        totalIndividuals += arrayNumberindividuals[i];
    }

    double * probabilitiesClasses = (double*) calloc(size,sizeof(double));
    for(int i = 0; i < size; i++){
        probabilitiesClasses[i] = arrayNumberindividuals[i] / (double) totalIndividuals;
    }

    double * cumulativeProbabilities = (double*) calloc(size,sizeof(double));
    cumulativeProbabilities[0] = probabilitiesClasses[0];
    for(int i = 1; i < size; i++){
        cumulativeProbabilities[i] = cumulativeProbabilities[i-1] + probabilitiesClasses[i];
    }

    double * simulation = (double*) calloc(3,sizeof(double));
    double genProb;
    for(int i = 0; i < drawings; i++){
        genProb = genrand_real1();
        int foundClass = 0;
        int j = 0;
        while(j < size && !foundClass){
            if(genProb <= cumulativeProbabilities[j]){
                simulation[j] += 1;
                foundClass = 1;
            }
            j++;
        }
    }
    for(int j = 0; j < size; j++) simulation[j] /= drawings;
    free(probabilitiesClasses);
    free(cumulativeProbabilities);
    return simulation;
}

/* -------------------------------------------------------------------- */
/*                           Question 4-a                               */
/* -------------------------------------------------------------------- */
/* negExp      Renvoie la moyenne obtenue des x après des itérations    */
/*             selon l'équation : x = -Mean * ln(1 - nombreAléatoire)   */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*            Mean la moyenne de l'équation                             */
/*                                                                      */
/* En sortie: la moyenne des x                                          */
/* -------------------------------------------------------------------- */

double negExp(double Mean, int drawings){
    double meanX = 0;
    for(int i = 0; i < drawings; i++){
        meanX += -Mean * log(1 - genrand_real2());
    }
    return meanX / (double) drawings;
}

/* -------------------------------------------------------------------- */
/*                           Question 4-c                               */
/* -------------------------------------------------------------------- */
/* disDistrib  Renvoie le tableau donnant à la case i le nombre         */
/*             d'occurences que negExp soit compris entre i et i+1      */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*                                                                      */
/* En sortie: le tableau des occurences des x                           */
/* -------------------------------------------------------------------- */

int * disDistrib(int drawings){
    int * Test22bin = (int *) calloc(22,sizeof(int));
    for(int i = 0; i < drawings; i++){
        int newX = (int) negExp(10,1);
        if(newX > 20){
            Test22bin[21] += 1;
        }
        else{
            Test22bin[newX] += 1;
        }
    }
    return Test22bin;
}

/* -------------------------------------------------------------------- */
/*                           Question 5-1                               */
/* -------------------------------------------------------------------- */
/* occurDice   Renvoie le tableau donnant le nombre d'occurences dans   */
/*             la case i qu'on ait obtenue la valeur 40 + i après un    */ 
/*             lancer de 40 dés                                         */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*                                                                      */
/* En sortie: le tableau des occurences des valeurs des 40 dés          */
/* -------------------------------------------------------------------- */

int * occurDice(int drawings){
    int * tabOccurences = (int *) calloc(201, sizeof(int));
    for(int i = 0; i < drawings; i++){
        int total = 0;
        for(int j = 0; j < 40; j++){
            total += (int) uniform(1,6);
        }
        tabOccurences[total - 40] += 1;
    }
    return tabOccurences;
}

/* -------------------------------------------------------------------- */
/*                           Question 5-2                               */
/* -------------------------------------------------------------------- */
/* BoxMuller   Renvoie le tableau donnant le nombre d'occurences        */
/*             case 0 : < -5                                            */
/*             case 19 : > 5                                            */
/*             case 1 : entre -5 et -4,5                                */
/*             case 2 : entre -4,5 et -4                                */
/*             ...                                                      */
/*                                                                      */
/* En entrée: drawings le nombre de simulation que l'on souhaite faire  */
/*                                                                      */
/* En sortie: le tableau des occurences des valeurs des 40 dés          */
/* -------------------------------------------------------------------- */

int * BoxMuller(int drawings){
    int * tabOccurences = (int *) calloc(20, sizeof(int));
    for(int i = 0; i < drawings; i++){
        double Rn1 = genrand_real1();
        double Rn2 = genrand_real1();
        double x1 = cos(2*M_PI*Rn2)*sqrt(-2*log2(Rn1));
        double x2 = sin(2*M_PI*Rn2)*sqrt(-2*log2(Rn1));
        if(x1 < -5){
            tabOccurences[0] += 1;
        }
        else if(x1 > 5){
            tabOccurences[19] += 1;
        }
        else{
            if((int) x1 == (int) round(x1)){
                tabOccurences[((int) x1 + 4) * 2 + 2] += 1;
            }
            else{
                tabOccurences[((int) x1 + 4) * 2 + 1] += 1;
            }
            
        }

        if(x2 < -5){
            tabOccurences[0] += 1;
        }
        else if(x2 > 5){
            tabOccurences[19] += 1;
        }
        else{
            if((int) x2 == (int) round(x2)){
                tabOccurences[((int) x2 + 4) * 2 + 2] += 1;
            }
            else{
                tabOccurences[((int) x2 + 4) * 2 + 1] += 1;
            }
        }
        
    }
    return tabOccurences;
}

/* -------------------------------------------------------------------- */
/*                           Question 6                                 */
/* -------------------------------------------------------------------- */
/*  C :                                                                 */
/*                                                                      */
/*  GSL (GNU Scientific Library) : Cette bibliothèque propose des       */
/*    fonctions pour générer des nombres aléatoires selon différentes   */
/*    distributions, y compris la loi normale et la loi exponentielle.  */
/*    Fonctions :                                                       */ 
/*    gsl_ran_gaussian (pour la loi normale),                           */
/*    gsl_ran_exponential (pour la loi exponentielle).                  */
/*                                                                      */
/*    C++ :                                                             */
/*                                                                      */
/*   <random> (bibliothèque standard de C++) : La bibliothèque C++      */
/*     standard contient tout ce qu'il faut pour générer des nombres    */
/*     aléatoires avec des distributions spécifiques.                   */
/*     Distributions :                                                  */
/*     std::normal_distribution (pour la loi normale),                  */
/*     std::exponential_distribution (pour la loi exponentielle).       */
/*                                                                      */
/*    Java :                                                            */
/*                                                                      */
/*    Java Standard Library                                             */
/*      (java.util.Random / java.util.concurrent.ThreadLocalRandom)     */
/*    Apache Commons Math : Fournit des fonctions pour la génération    */
/*      de nombres aléatoires selon diverses distributions.             */
/*            Classes : NormalDistribution, ExponentialDistribution.    */
/* -------------------------------------------------------------------- */

int main(){
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    /*
    //Question 2
    double tab[100000];
    for(int i = 0; i < 100000; i++){
        tab[i] = uniform(-98, 57.7);
    }
    printf("Moyenne : %f, écart-type : %f",meanArrayDouble(tab,100000,100000), standDeviation(tab, 100000, 100000));
    printf("\n");
    */

    /*
    //Question 3-a
    double * simuBase = simulate(1000);
    afficheTableauDouble(simuBase,3);
    simuBase = simulate(10000);
    afficheTableauDouble(simuBase,3);
    simuBase = simulate(100000);
    afficheTableauDouble(simuBase,3);
    simuBase = simulate(1000000);
    afficheTableauDouble(simuBase,3);
    free(simuBase);
    */

    /*
    //Question 3-b
    double classesPopulation[] = {350.0, 450.0, 200.0};
    double * simuGene = simGeneral(1000, 3, classesPopulation);
    afficheTableauDouble(simuGene,3);
    simuGene = simGeneral(1000000, 3, classesPopulation);
    afficheTableauDouble(simuGene,3);
    free(simuGene);
    */

    /*
    //Question 4-b
    printf("%f\n",negExp(10, 1000));
    printf("%f\n",negExp(10, 1000000));
    */

    /*
    //Question 4-c
    int * diceDistrib;
    diceDistrib = disDistrib(1000000);
    afficheTableauInt(diceDistrib,22);
    printf("Moyenne  : %f, Ecart-type : %f\n", meanArrayOccur(diceDistrib, 22, 1000000, 0, 1), standDeviationOccur(diceDistrib, 22, 1000000, 0, 1));
    free(diceDistrib);
    */

    /*
    //Question 5-1
    int * diceRolls = occurDice(1000000);
    afficheTableauInt(diceRolls, 201);
    printf("Moyenne  : %f, Ecart-type : %f\n", meanArrayOccur(diceRolls, 201, 1000000, 40, 1), standDeviationOccur(diceRolls, 201, 1000000, 40, 1));
    free(diceRolls);
    */

    /*
    //Question 5-2
    int * testBoxMuller = BoxMuller(1000);
    afficheTableauInt(testBoxMuller,20);
    printf("Moyenne  : %f, Ecart-type : %f\n", meanArrayOccur(testBoxMuller, 22, 1000, -5, 0.5), standDeviationOccur(testBoxMuller, 22, 1000, -5, 0.5));
    testBoxMuller = BoxMuller(1000000);
    afficheTableauInt(testBoxMuller,20);
    printf("Moyenne  : %f, Ecart-type : %f\n", meanArrayOccur(testBoxMuller, 22, 1000000, -5, 0.5), standDeviationOccur(testBoxMuller, 22, 1000000, -5, 0.5));
    free(testBoxMuller);
    */

    return 0;
}
#ifndef TP2_H
#define TP2_H

#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#define _USE_MATH_DEFINES

double uniform(double inA, double inB);

double * simulate(int drawings);

double * simGeneral(int drawings, int size, double * arrayNumberindividuals);

double negExp(double Mean, int drawings);

int * disDistrib(int drawings);

int * occurDice(int drawings);

int * BoxMuller(int drawings);

#endif
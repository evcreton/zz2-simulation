#include "arraySimu.h"

/* -------------------------------------------------------------------- */
/* afficheTableau    Affiche le contenu d'un tableau de taille N        */
/*                                                                      */
/* En entrée: tab un tableau                                            */
/*            size la taille du tableau                                 */
/* -------------------------------------------------------------------- */

void afficheTableauDouble(double tab[], int size){
    printf("| ");
    for(int i  = 0; i < size; i++){
        printf("%f | ", tab[i]);
    }
    printf("\n\n");
}

void afficheTableauInt(int tab[], int size){
    printf("| ");
    for(int i  = 0; i < size; i++){
        printf("%d | ", tab[i]);
    }
    printf("\n\n");
}


/* -------------------------------------------------------------------- */
/* meanArrayDouble   Renvoie la moyenne d'un tableau de taille N        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: la moyenne du tableau                                     */
/* -------------------------------------------------------------------- */

double meanArrayDouble(double arrayOccurences[], int size, int drawings){
    double mean = 0;
    for(int i = 0; i < size; i++){
        mean += arrayOccurences[i];
    }
    return mean / (double) drawings;
} 

/* -------------------------------------------------------------------- */
/* meanArrayOccur Renvoie la moyenne d'un tableau d'occurrence          */
/*                de taille N                                           */
/*                                                                      */
/* En entrée: tab un tableau d'entier                                   */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*            valDep la valeur de départ (ex: si la                     */
/*            première case du tableau correspond à -4)                 */
/*            pas le pas entre les cases du tableau                     */
/*                                                                      */
/* En sortie: la moyenne du tableau                                     */
/* -------------------------------------------------------------------- */

double meanArrayOccur(int arrayOccurences[], int size, int drawings, double valDep, double pas){
    double mean = 0;
    double val = valDep;
    for(int i = 0; i < size; i++){
        mean += (double) arrayOccurences[i] * (val);
        val += pas;
    }
    return mean / (double) drawings;
} 

/* -------------------------------------------------------------------- */
/* standDeviation  Renvoie l'écart tupe d'un tableau                    */
/*                 de valeurs de taille N                               */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: l'écart type du tableau                                   */
/* -------------------------------------------------------------------- */

double standDeviation(double tab[], int size, int drawings){
    double mean = meanArrayDouble(tab, size, drawings);
    double total = 0;
    for(int i = 0; i < size; i++){
        total += (tab[i] - mean) * (tab[i] - mean);
    }
    return sqrt(total / drawings);
}

/* -------------------------------------------------------------------- */
/* standDeviationOccur  Renvoie l'écart tupe d'un tableau               */
/*                      d'occurences de taille N                        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*            valDep la valeur de départ (ex: si la                     */
/*            première case du tableau correspond à -4)                 */
/*            pas le pas entre les cases du tableau                     */
/*                                                                      */
/* En sortie: l'écart type du tableau                                   */
/* -------------------------------------------------------------------- */

double standDeviationOccur(int tab[], int size, int drawings, double valDep, double pas){
    double mean = meanArrayOccur(tab, size, drawings, valDep, pas);
    double total = 0;
    double val = valDep;
    for(int i = 0; i < size; i++){
        total += tab[i] * ((val - mean) * (val - mean));
        val += pas;
    }
    return sqrt(total / drawings);
}
#include <stdio.h>
#include <math.h>
#include "mt19937ar.h"
#include "arraySimu.h"
#define _USE_MATH_DEFINES

/* -------------------------------------------------------------------- */
/*                           Question 1/2                               */
/* -------------------------------------------------------------------- */
/* simPI      Renvoie un réel approximant la valeur de pi               */
/*                                                                      */
/* En entrée: itérations le nombre d'iterations                         */
/*                                                                      */
/* En sortie: un réel approximant la valeur de pi                       */
/* -------------------------------------------------------------------- */

double simPi(int iterations){
    int pointInCircle = 0;

    for(int i = 0; i < iterations; i++){
        double x = genrand_real1();
        double y = genrand_real2();
        if(x*x + y*y < 1){
            pointInCircle++;
        }
    }
    return (double) 4 * pointInCircle / iterations;
}

/* -------------------------------------------------------------------- */
/* iterUntilPi  Renvoie le nombre d'iteration necessaire pour           */
/*              approximer à 10^(-precision) près                       */
/*                                                                      */
/* En entrée: precision la precision pour l'approxiamtion               */
/*                                                                      */
/* En sortie: un entier representatn le nombre d'iterations             */
/* -------------------------------------------------------------------- */

int iterUntilPi(int precision){
    double error = pow(10, -(precision + 1));
    double iterations = 0;
    double meanPi = 0;

    do{
        meanPi += simPi(1);
        iterations++;
    }while(fabs(meanPi / iterations - M_PI) > error);

    // printf("%f\n", meanPi / iterations);
    return iterations;
}

int main(){
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    /*
    //Question 1
    int N = 10;
    int tabIterPrecision[10] = {0};
    for(int p = 2; p < 5; p++){
        for(int i = 0; i < N; i++){
            tabIterPrecision[i] = iterUntilPi(p);
        }
        afficheTableauInt(tabIterPrecision, N);
        printf("moyenne d'iterations pour atteindre une précision à %d chiffre après la virgule de Pi : %f\n\n", p, meanArrayInt(tabIterPrecision, N, N));
    }
    */

    /*
    //Question 2
    int N = 1000;
    double meanPi = 0;
    for(int i = 0; i < 40; i++){
        meanPi += simPi(N);
    }
    meanPi /= 40;
    double absoluteError = fabs(meanPi - M_PI);
    printf("%d itérations : %f, difference avec M_PI : %f | %f %%\n", N, meanPi, absoluteError,absoluteError / M_PI * 100);

    N= 1000000;
    meanPi = 0;
    for(int i = 0; i < 25; i++){
        meanPi += simPi(N);
    }
    meanPi /= 25;
    absoluteError = fabs(meanPi - M_PI);
    printf("%d itérations : %f, difference avec M_PI : %f | %f %%\n", N, meanPi, absoluteError,absoluteError / M_PI * 100);
    
    N = 1000000000;
    meanPi = 0; 
    for(int i = 0; i < 10; i++){
        meanPi += simPi(N);
    } 
    meanPi /= 10;
    absoluteError = fabs(meanPi - M_PI);
    printf("%d itérations : %f, difference avec M_PI : %f | %f %%\n", N, meanPi, absoluteError,absoluteError / M_PI * 100);
    */
    
    
    //Question 3
    int N = 1000;
    double tabPi[40] = {0}; 
    for(int i = 0; i < 40; i++){
        tabPi[i] = simPi(N);
    }

    double * intervalle;
    intervalle = confidenceInterval05(tabPi, 40, 40);
    afficheTableauDouble(intervalle,2);
    free(intervalle);
    /**/

    return 0;
}
#include "arraySimu.h"

/* -------------------------------------------------------------------- */
/* afficheTableau    Affiche le contenu d'un tableau de taille N        */
/*                                                                      */
/* En entrée: tab un tableau                                            */
/*            size la taille du tableau                                 */
/* -------------------------------------------------------------------- */

void afficheTableauDouble(double tab[], int size){
    printf("| ");
    for(int i  = 0; i < size; i++){
        printf("%f | ", tab[i]);
    }
    printf("\n\n");
}

void afficheTableauInt(int tab[], int size){
    printf("| ");
    for(int i  = 0; i < size; i++){
        printf("%d | ", tab[i]);
    }
    printf("\n");
}


/* -------------------------------------------------------------------- */
/* meanArrayDouble   Renvoie la moyenne d'un tableau de taille N        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: la moyenne du tableau                                     */
/* -------------------------------------------------------------------- */

double meanArrayDouble(double arrayValues[], int size, int drawings){
    double mean = 0;
    for(int i = 0; i < size; i++){
        mean += arrayValues[i];
    }
    return mean / (double) drawings;
} 

/* -------------------------------------------------------------------- */
/* meanArrayInt   Renvoie la moyenne d'un tableau de taille N           */
/*                                                                      */
/* En entrée: tab un tableau d'entiers                                  */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: la moyenne du tableau                                     */
/* -------------------------------------------------------------------- */

double meanArrayInt(int arrayValues[], int size, int drawings){
    double mean = 0;
    for(int i = 0; i < size; i++){
        mean += arrayValues[i];
    }
    return mean / (double) drawings;
} 

/* -------------------------------------------------------------------- */
/* meanArrayOccur Renvoie la moyenne d'un tableau d'occurrence          */
/*                de taille N                                           */
/*                                                                      */
/* En entrée: tab un tableau d'entier                                   */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*            valDep la valeur de départ (ex: si la                     */
/*            première case du tableau correspond à -4)                 */
/*            pas le pas entre les cases du tableau                     */
/*                                                                      */
/* En sortie: la moyenne du tableau                                     */
/* -------------------------------------------------------------------- */

double meanArrayOccur(int arrayOccurences[], int size, int drawings, double valDep, double pas){
    double mean = 0;
    double val = valDep;
    for(int i = 0; i < size; i++){
        mean += (double) arrayOccurences[i] * (val);
        val += pas;
    }
    return mean / (double) drawings;
} 

/* -------------------------------------------------------------------- */
/* standDeviation  Renvoie l'écart type d'un tableau                    */
/*                 de valeurs de taille N                               */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: l'écart type du tableau                                   */
/* -------------------------------------------------------------------- */

double standDeviation(double tab[], int size, int drawings){
    double mean = meanArrayDouble(tab, size, drawings);
    double total = 0;
    for(int i = 0; i < size; i++){
        total += (tab[i] - mean) * (tab[i] - mean);
    }
    return sqrt(total / drawings);
}

/* -------------------------------------------------------------------- */
/* standDeviationOccur  Renvoie l'écart tupe d'un tableau               */
/*                      d'occurences de taille N                        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*            valDep la valeur de départ (ex: si la                     */
/*            première case du tableau correspond à -4)                 */
/*            pas le pas entre les cases du tableau                     */
/*                                                                      */
/* En sortie: l'écart type du tableau                                   */
/* -------------------------------------------------------------------- */

double standDeviationOccur(int tab[], int size, int drawings, double valDep, double pas){
    double mean = meanArrayOccur(tab, size, drawings, valDep, pas);
    double total = 0;
    double val = valDep;
    for(int i = 0; i < size; i++){
        total += tab[i] * ((val - mean) * (val - mean));
        val += pas;
    }
    return sqrt(total / drawings);
}

/* -------------------------------------------------------------------- */
/* varianceWithoutBias  Renvoie la variance sans biais d'un tableau     */
/*                      de valeurs de taille N                          */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: la variance sans biais                                    */
/* -------------------------------------------------------------------- */

double varianceWithoutBias(double tab[], int size, int drawings){
    double mean = meanArrayDouble(tab, size, drawings);
    double variance = 0;
    for(int i = 0; i < size; i++){
        variance += (tab[i] - mean) * (tab[i] - mean);
    }
    return variance / (drawings - 1);
}

/* -------------------------------------------------------------------- */
/* confidenceInterval05  Renvoie un tableau de deux cases représentant  */
/*                       l'intervalle de confiance à 95% d'une loi de   */
/*                       Student                                        */
/*                                                                      */
/* En entrée: tab un tableau de flottants                               */
/*            size la taille du tableau                                 */
/*            drawings le nombre d'occurences                           */
/*                                                                      */
/* En sortie: l'intervalle de confiance                                 */
/* -------------------------------------------------------------------- */

double * confidenceInterval05(double tab[], int size, int drawings){
    double t[30] = {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.308, 2.262, 2.228, 
                     2.201, 2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086,
                     2.080, 2.074, 2.069, 2.064, 2.060, 2.056, 2.052, 2.048, 2.045, 2.042};
    double * interval = (double *) calloc(2, sizeof(double));
    double mean = meanArrayDouble(tab, size, drawings);
    double variance = varianceWithoutBias(tab, size, drawings);
    double t_student = 0;
    if(drawings > 30){
        if(drawings == 40) t_student = 2.021;
        else if(drawings == 80) t_student = 2.000;
        else if(drawings == 120) t_student = 1.980;
        else if(drawings >= 999999999) t_student = 1.960;
    } 
    else {
        t_student = t[drawings + 1];
    }

    double R = t_student * sqrt(variance / drawings);
    printf("%f\n", R);
    interval[0] = mean - R;
    interval[1] = mean + R;
    return interval;
}
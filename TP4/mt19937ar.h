#ifndef MT19937AR_H
#define MT19937AR_H

#include "math.h"

void init_genrand(unsigned long s);

void init_by_array(unsigned long init_key[], int key_length);

unsigned long genrand_int32(void);

double genrand_real1(void);

double genrand_real2(void);

double gen_gaussian(int center);

#endif
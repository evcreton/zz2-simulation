#ifndef TP4_H
#define TP4_H

#include "rabbit.h"

void showRabbitPopulationInt(int * currentSituation);

void simulationRabbit(int * initialSituation, int nbMonth, int boolShow);

void showRabbitPopulation(t_rabbitpopulation * population);

int toDie(t_rabbitpopulation * population, int index);

t_rabbitpopulation * initializePopulation(int nbMales, int nbFemales);

void createAdultFemale(t_rabbitpopulation * population);

void createAdultMale(t_rabbitpopulation * population);

void createChild(t_rabbitpopulation * population);

void toAgeOld(t_rabbit * curr_rabbit);

void toAgeChildOrAdult(t_rabbit * curr_rabbit);

void toAgeBaby(t_rabbit * curr_rabbit);

void toBirthMale(t_rabbitpopulation * population, int index);

void toBirthLady(t_rabbitpopulation * population, int index);

void toBirthChild(t_rabbitpopulation * population, int index);

void evolvePopulation(t_rabbitpopulation * population);

void simulatePopulation(int months, int nbMales, int nbFemales);

#endif
#include <stdio.h>
#include <stdlib.h>
#include "mt19937ar.h"



typedef struct t_rabbitpopulation {
    int currentMounth;
    int nbRabbits;
    int nb_RabbitsMale;
    struct t_rabbit * rabbit[1000000];
} t_rabbitpopulation;

typedef struct t_rabbit {
    char sexe;
    int age;
    int maturity;
    double survivalRate;
    int nbLittersThisYear;
    void (*toAge)(struct t_rabbit *);
    void (*toBirth)(struct t_rabbitpopulation *, int);
} t_rabbit;


#include "TP4.h"

/* -------------------------------------------------------------------- */
/* showRabbitPopulationInt    Affiche la population de lapins           */
/*                                                                      */
/* En entrée: currentSituation un tableau d'entier de deux cases        */
/* -------------------------------------------------------------------- */

void showRabbitPopulationInt(int * currentSituation){
    for(int oL = 0; oL < currentSituation[0]; oL++) printf("LL ");
    for(int sL = 0; sL < currentSituation[1]; sL++) printf("ll ");
    printf("\n\n");
}

/* -------------------------------------------------------------------- */
/* simulationRabbit    simule basiquement l'évolution d'une popoulation */
/*                     de lapins pendants un nombre de mois             */
/*                                                                      */
/* En entrée: initialSituation un tableau d'entier de deux cases        */
/*            nbMonth un entier représentant le nombre de mois pour la  */
/*            simulation                                                */
/*            boolShow un booléen true si l'on souhaite afficher        */
/*            l'évolution de la population chaque mois                  */
/* -------------------------------------------------------------------- */

void simulationRabbit(int * initialSituation, int nbMonth, int boolShow){
    if(boolShow) showRabbitPopulationInt(initialSituation);
    for(int m = 0; m < nbMonth; m++){
        int nbNewOld = initialSituation[1];
        initialSituation[1] = initialSituation[0];
        initialSituation[0] += nbNewOld;
        if(boolShow) showRabbitPopulationInt(initialSituation);
    }
}

/*-----------------------------------------------------------------------------------------------------------------------------------*/

/* -------------------------------------------------------------------- */
/* showRabbitPopulation    Affiche la population de lapins              */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void showRabbitPopulation(t_rabbitpopulation * population){
    int total = population->nbRabbits;
    for(int i = 0; i < total; i++){
        t_rabbit * currRabit = population->rabbit[i];
        if(currRabit != NULL){
            if(currRabit->sexe == 'f'){
                if(currRabit->age >= currRabit->maturity) printf("F ");
                else printf("f ");
            }
            else {
                if(currRabit->age >= currRabit->maturity) printf("H ");
                else printf("h ");
            }
        }
    }
    printf("\n\n");
}

/* -------------------------------------------------------------------- */
/* showRabbitMinimalPopulation    Affiche la population de lapins       */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void showRabbitMinimalPopulation(t_rabbitpopulation * population){
    long int total = population->nbRabbits;
    int pop[4] = {0,0,0,0};
    for(int i = 0; i < total; i++){
        t_rabbit * currRabit = population->rabbit[i];
        if(currRabit != NULL){
            if(currRabit->sexe == 'f'){
                if(currRabit->age >= currRabit->maturity) pop[0] += 1;
                else pop[1] += 1;
            }
            else {
                if(currRabit->age >= currRabit->maturity) pop[2] += 1;
                else pop[3] += 1;
            }
        }
    }
    printf("F: %d, f: %d, H: %d, h: %d", pop[0], pop[1], pop[2], pop[3]);
    printf("\n\n");
}

/* -------------------------------------------------------------------- */
/* initializePopulation    initialize la population de lapins avec un   */
/*                         certain nombre de lapins males et femelles   */
/*                                                                      */
/* En entrée: nbMales un entier donnant le nombres de males que l'on    */
/*            souhaite mettre dans la population de lapins              */
/*            nbFemales un entier donnant le nombres de femelles que    */
/*            l'on souhaite mettre dans la population de lapins         */
/*                                                                      */
/* En sortie: t_rabbitpopulation correctement initializé                */
/* -------------------------------------------------------------------- */

t_rabbitpopulation * initializePopulation(int nbMales, int nbFemales){
    t_rabbitpopulation * population = (t_rabbitpopulation *) malloc(sizeof(t_rabbitpopulation));
    population->nbRabbits = 0;
    population->nb_RabbitsMale = 0;
    population->currentMounth = 0;
    for(int r = 0; r < 1000000; r++) population->rabbit[r] = NULL;
    for(int r = 0; r < nbMales; r++) createAdultMale(population);
    for(int r = 0; r < nbFemales; r++) createAdultFemale(population);
    return population;
}

/* -------------------------------------------------------------------- */
/* createAdultFemale   Créer une femelle lapin dans la population       */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void createAdultFemale(t_rabbitpopulation * population){ // pour initialization seulement
    t_rabbit * ladyRabbit = (t_rabbit *) malloc(sizeof(t_rabbit));
    ladyRabbit->age = 13, ladyRabbit->sexe = 'f', ladyRabbit->survivalRate = 0.6, ladyRabbit->maturity = 5, ladyRabbit->toAge = toAgeChildOrAdult, ladyRabbit->toBirth = toBirthLady;
    population->rabbit[population->nbRabbits] = ladyRabbit;
    population->nbRabbits += 1;
}

/* -------------------------------------------------------------------- */
/* createAdultMale   Créer un lapin malin dans la population            */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void createAdultMale(t_rabbitpopulation * population){ // pour initialization seulement
    t_rabbit * maleRabbit = (t_rabbit *) malloc(sizeof(t_rabbit));
    maleRabbit->age = 13, maleRabbit->sexe = 'h', maleRabbit->survivalRate = 0.6, maleRabbit->maturity = 5, maleRabbit->toAge = toAgeChildOrAdult, maleRabbit->toBirth = toBirthMale;
    population->rabbit[population->nbRabbits] = maleRabbit;
    population->nbRabbits += 1;
    population->nb_RabbitsMale += 1;
}

/* -------------------------------------------------------------------- */
/* createChild   Créer un lapereau dans la population                   */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void createChild(t_rabbitpopulation * population){
    t_rabbit * newRabbit = (t_rabbit *) malloc(sizeof(t_rabbit));
    newRabbit->age = 0;
    newRabbit->survivalRate = 0.35;
    newRabbit->toAge = toAgeBaby;
    newRabbit->maturity = 5 + (8 - 5) * genrand_real1();
    newRabbit->nbLittersThisYear = 0;
    double maleOrFemale = genrand_real1();
    if(maleOrFemale > 0.5){
        newRabbit->sexe = 'f';
        newRabbit->toBirth = toBirthChild;
    }
    else{
        newRabbit->sexe = 'h';
        newRabbit->toBirth = toBirthMale;
        population->nb_RabbitsMale += 1;
    }
    population->rabbit[population->nbRabbits] = newRabbit;
    population->nbRabbits += 1;
}

/* -------------------------------------------------------------------- */
/* toDie    détermine si le lapin à l'index meurt ou non                */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/*            index un entier pour l'index du lapin concerné            */
/*                                                                      */
/* En sortie: un booléen 1 si le lapin est mort 0 sinon                 */
/* -------------------------------------------------------------------- */

int toDie(t_rabbitpopulation * population, int index){
    double ndRand = genrand_real1();
    int isDead = 0;
   
    if(population->rabbit[index]->age % 12 == 0 && population->rabbit[index]->survivalRate < ndRand){
        isDead = 1;
        if(population->rabbit[index]->sexe == 'h') population->nb_RabbitsMale -= 1;
        free(population->rabbit[index]);
        population->nbRabbits -= 1;
        
        population->rabbit[index] = population->rabbit[population->nbRabbits];
        population->rabbit[population->nbRabbits] = NULL;
    }
    return isDead;
}

/* -------------------------------------------------------------------- */
/* toAgeOld  fonction définissant le type de vieillissement pour un     */
/*           lapin donné, pour un lapin de 10 ans ou plus               */
/*                                                                      */
/* En entrée: le lapin qui est entrain de vieillir                      */
/* -------------------------------------------------------------------- */

void toAgeOld(t_rabbit * curr_rabbit){
    curr_rabbit->age += 1;
    if(curr_rabbit->age % 12 == 0) curr_rabbit->survivalRate -= 0.1;
}

/* -------------------------------------------------------------------- */
/* toAgeChildOrAdult  fonction définissant le type de vieillissement    */
/*                    pour un lapin donné, pour un lapin jeune ou adulte*/
/*                                                                      */
/* En entrée: le lapin qui est entrain de vieillir                      */
/* -------------------------------------------------------------------- */

void toAgeChildOrAdult(t_rabbit * curr_rabbit){
    curr_rabbit->age += 1;
    if(curr_rabbit->age == 119) curr_rabbit->toAge = toAgeOld;
}

/* -------------------------------------------------------------------- */
/* toAgeBaby  fonction définissant le type de vieillissement pour un    */
/*            lapin donné, pour un lapin venat de naître                */
/*                                                                      */
/* En entrée: le lapin qui est entrain de vieillir                      */
/* -------------------------------------------------------------------- */

void toAgeBaby(t_rabbit * curr_rabbit){
    curr_rabbit->toAge = toAgeChildOrAdult;
}

/* -------------------------------------------------------------------- */
/* toBirthMale    fonction déterminant si le lapin peut donné naissance */
/*                ou non, impossible dans ce cas là un lapin n'est      */
/*                pas un hippocampe                                     */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/*            index un entier pour l'index du lapin concerné            */
/* -------------------------------------------------------------------- */

void toBirthMale(t_rabbitpopulation * population, int index){ // Je pensais pas écrire ce nom de fonction un jour ;D
    (void) index;
    (void) population;
}

/* -------------------------------------------------------------------- */
/* toBirthLady    fonction déterminant si le lapin peut donné naissance */
/*                ou non, pour une lapine femelle ayant la maturité     */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/*            index un entier pour l'index du lapin concerné            */
/* -------------------------------------------------------------------- */

void toBirthLady(t_rabbitpopulation * population, int index){
    if(population->rabbit[index]->age % 12 == 0 && population->nb_RabbitsMale > 0){
        population->rabbit[index]->nbLittersThisYear = (int) (gen_gaussian(6));
    }
    double probaBirthPerMounth[12] = {0.2, 0.15, 0.2, 0.35, 0.45, 0.7, 0.75, 0.5, 0.4, 0.25, 0.15, 0.1};
    double probaThisMounth = genrand_real1();
    if ((population->nb_RabbitsMale != 0) && ((12 - (population->rabbit[index]->age % 12) == population->rabbit[index]->nbLittersThisYear) || probaBirthPerMounth[population->currentMounth%12] > probaThisMounth)){
        int nbKittens = (int) 3 + 3 * genrand_real1();
        for(int i = 0; i < nbKittens; i++) createChild(population);
        population->rabbit[index]->nbLittersThisYear--;
    }
}

/* -------------------------------------------------------------------- */
/* toBirthChild    fonction déterminant si le lapin peut donné          */
/*                 naissance ou non, pour une lapine femelle n'ayant    */
/*                 pas la maturité                                      */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/*            index un entier pour l'index du lapin concerné            */
/* -------------------------------------------------------------------- */

void toBirthChild(t_rabbitpopulation * population, int index){ // Bizarre le nom de la fonction
    t_rabbit * courRabbit = population->rabbit[index];
    if(courRabbit->age == courRabbit->maturity) courRabbit->toBirth = toBirthLady;
}

/* -------------------------------------------------------------------- */
/* evolvePopulation    fait évoluer tous les lapins de la               */
/*                     population d'un mois                             */
/*                                                                      */
/* En entrée: population représentant la population de lapins           */
/* -------------------------------------------------------------------- */

void evolvePopulation(t_rabbitpopulation * population){
    int index = 0;
    int nbMort = 0;
    while(population->rabbit[index] != NULL){
        t_rabbit * courRabbit = population->rabbit[index];
        if(!toDie(population, index)){
            courRabbit->toAge(courRabbit);
            courRabbit->toBirth(population, index);
            index++;
        }
        else{
            nbMort++;
        }
    }
    printf("Mort ce mois: %d ", nbMort);
} 

/* -------------------------------------------------------------------- */
/* simulatePopulation    simule pendant un nombre de mois l'évolution   */
/*                       d'une population initiale de lapins            */
/*                                                                      */
/* En entrée: nbMales un entier donnant le nombres de males que l'on    */
/*            souhaite mettre dans la population de lapins              */
/*            nbFemales un entier donnant le nombres de femelles que    */
/*            l'on souhaite mettre dans la population de lapins         */
/*            months la duréen en mois de la simulation                 */
/* -------------------------------------------------------------------- */

void simulatePopulation(int months, int nbMales, int nbFemales){
    int i = 1;
    printf("Month 0 :\n");
    t_rabbitpopulation * population = initializePopulation(nbMales,nbFemales);
    showRabbitMinimalPopulation(population);
    while(population->rabbit[0] != NULL && i <= months){
        evolvePopulation(population);
        printf("Month %d :\n", i);
        showRabbitMinimalPopulation(population);
        i++;
        population->currentMounth++;
    }
    
    for(int r = 0; r < population->nbRabbits; r++) free(population->rabbit[r]);
    free(population);
}

int main(){
    /*
    //Question 1
    int initialSituation[2] = {0,1};
    simulationRabbit(initialSituation, 12, 1);
    */
    unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
    init_by_array(init, length);
    simulatePopulation(252, 1, 1);
    //simulatePopulation(252, 10, 10);
    return 0;
}